module.exports = {
  mongoUri: process.env.mongoUri,
  port: process.env.PORT,
  secret: process.env.secret
}