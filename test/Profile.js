process.env.NODE_ENV = 'dev';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const server = require('../server');
const Users = require('../models/Users');
const expect = chai.expect;
const userName = 'Rupeshiya';
const password = 'abc123';
const email = "test4@test.com";
const userId = '5d737845adecce2ddcd8983a';
const phone = "9123492988";
var loggedInToken = '';
var profileId = '';
const async = require('async');
const Profile = require('../models/Profile');

chai.use(chaiHttp);

function logInUser(done){
  chai.request(server)
    .post('/api/users/login')
    .send({
      email: email,
      password: password
    })
    .end(function(err, res){
      loggedInToken = res.body.token;
      console.log(loggedInToken);
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.have.property('token');
      expect(res.body).to.include({success: true});
      done();
    })
}

function createProfile(done){
  const profileInfo = {
    founder: "Rupeshiya",
    category: "fun",
    objective: "to have fun",
    details: "to have fun"
  };

  chai.request(server)
    .post('/api/profile/create')
    .set('Authorization', loggedInToken)
    .send(profileInfo)
    .end(function(err, res){
      if(err){
        console.log(err);
      }
      profileId = res.body.response._id;
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.include({success: true});
      done();
    });
}

// get profile of id 
function getProfile(done){
  chai.request(server)
  .get(`/api/profile/${profileId}`)
  .set('Authorization', loggedInToken)
  .end(function(err, res){
    expect(res.body).to.be.an('object');
    expect(res.body).to.have.property('success');
    expect(res.body).to.include({success: true});
    done();
  });
}
// update the profile 
function updateProfile(done){
  const updateInfo = {
    founder: "Rupeshiya",
    category: "fun edit test",
    objective: "to have fun edit test",
    details: "to have fun edit test"
  };
  chai.request(server)
  .put('/api/profile/edit')
  .set('Authorization', loggedInToken)
  .send(updateInfo)
  .end(function(err, res){
    expect(res.body).to.be.an('object')
    expect(res.body).to.have.property('success');
    expect(res.body).to.include({success: true});
    // after update delete the profile
    Profile.findByIdAndRemove(profileId)
      .then(()=>{
        done();
      });
  })  
}

// grouping all tests 
describe('profile tests :', ()=>{
  
  // before all test login 
  before((done)=>{
    logInUser(done)
  });

  // should create the profile 
  it('should create the profile', (done)=>{
    createProfile(done);
  });

  // /api/profile/:profileId
  it('should get the profile of id ', (done)=>{
    getProfile(done);
  });

  // update the profile 
  it('should update the profile', (done)=>{
    updateProfile(done);
  })

})