process.env.NODE_ENV = 'dev';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const server = require('../server');
const Users = require('../models/Users');
const expect = chai.expect;
const userName = 'Rupeshiya';
const password = 'abc123';
const email = "test4@test.com";
const userId = '5d737845adecce2ddcd8983a';
const phone = "9123492988";
var loggedInToken = '';
const postId = "5d544893c5d4fc457b2f1f9b";
var createPostId = '';
const async = require('async');

chai.use(chaiHttp);

// login user for token 
function logInUser(done){
  chai.request(server)
    .post('/api/users/login')
    .send({
      email: email,
      password: password
    })
    .end(function(err, res){
      loggedInToken = res.body.token;
      console.log(loggedInToken);
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.have.property('token');
      expect(res.body).to.include({success: true});
      done();
    })
}

// get all posts 
function getPosts(done){
  chai.request(server)
    .get('/api/project/posts')
    .set('Authorization', loggedInToken)
    .end(function(err, res){
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.include({success: true});
      done();
    });
}

// fn to get post by id 
function getPostById(done){
  chai.request(server)
  .get(`/api/project/post/${postId}`)
  .set('Authorization', loggedInToken)
  .end(function(err, res){
    expect(res.body).to.be.an('object');
    expect(res.body).to.have.property('success');
    expect(res.body).to.include({success: true});
    done();
  })
}

// create posts 
function createPost(done){
  const postInfo = {
    title: "testing create test",
    description: "description ",
    requiredFunding: 3000
  };
  chai.request(server)
    .post('/api/project/create')
    .set('Authorization', loggedInToken)
    .send(postInfo)
    .end(function(err, res){
      createPostId = res.body.post._id;
      console.log(createPostId);
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.include({success: true});
      done();
    })
}

// function to delete the posts 
function deletePostById(done){
  console.log(createPostId);
  chai.request(server)
    .delete(`/api/project/posts/${createPostId}`)
    .set('Authorization', loggedInToken)
    .end(function(err, res){
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.have.property('msg');
      done();
    })
}

// grouping all tests 
describe('Testing posts : -', ()=>{
  before(function(done){
    logInUser(done);
  });

  // get all posts 
  it('should give all the posts : -', (done)=>{
    getPosts(done);
  })

  // get post by id 
  it('should give the post of id 5d544893c5d4fc457b2f1f9b: -', (done)=>{
    getPostById(done);
  });

  // should create a post
  it('should create a post : ', (done)=>{
    createPost(done);
  });

  // delete posts 
  it('should delete the post : -', (done)=>{
    deletePostById(done);
  });

})