process.env.NODE_ENV = 'dev';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const server = require('../server');
const Users = require('../models/Users');
const expect = chai.expect;
const userName = 'Rupeshiya';
const password = 'abc123';
const email = "test4@test.com";
const userId = '5d737845adecce2ddcd8983a';
const phone = "9123492988";
var loggedInToken = '';
const async = require('async');

chai.use(chaiHttp);
function loginUser(done) {
  chai.request(server)
    .post('/api/users/login')
    .send({
      email: "test3@test.com",
      password: "abc123"
    })
    .end(function(err, res){
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.have.property('token');
      expect(res.body).to.include({success: true });
      done();
    });
}

// to register the users
function registerUsers(done){
  chai.request(server)
   .post('/api/users/register')
   .send({
     email: email,
     password: password,
     name: userName,
     phone: phone
   })
   .end(function(err, res){
     expect(res.body).to.be.an('object');
     expect(res.body).to.include({success: true});
     expect(res.body).to.have.property('success');
     done();
   });
}

describe('Users-', ()=>{
  before(function(done){
    loginUser(done);
  });

  it('should login the user: -', (done)=>{
    loginUser(done);
  });

  it('should register the user: -', (done)=>{
    registerUsers(done);
  })
});
