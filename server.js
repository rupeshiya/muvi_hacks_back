require('dotenv').config()
const express = require('express');
const app = express();
const keys = require('./config/keys');
const PORT = process.env.PORT || keys.port;
const mongoose = require('mongoose');
const users = require('./routes/users');
const project = require('./routes/project');
const profile = require('./routes/profile');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
const path = require('path');
const compressor = require('compression');

//  connect mongo
mongoose.connect(process.env.mongoUri, {
    useNewUrlParser: true
  })
  .then(() => {
    console.log('mongo connected');
  })
  .catch((err) => {
    console.log('mongo error',err);
  });

app.use(compressor());

app.use(passport.initialize());
require('./config/passport')(passport);

app.use(cors());

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get('/',(req, res)=>{
  res.json({success: true, msg: 'home route worked'})
})


// use routers
app.use('/api/users', users);
app.use('/api/project', project);
app.use('/api/profile',profile);

app.listen(PORT, () => {
  console.log(`listening on ${PORT}`);
});

module.exports = app;