const express = require('express');
const passport = require('passport');
const Profile = require('../models/Profile');
const router = express.Router();
const { client } = require('../routes/redis');

// /api/profile/test
router.get('/test',(req, res)=>{
  res.json({success: true, msg: 'test route works'});
});


// create profile 
// /api/profile/create 
// private 

router.post('/create',passport.authenticate('jwt',{ session: false }),(req, res)=>{
  const founder = escape(req.body.founder);
  const category = escape(req.body.category);
  const objective = escape(req.body.objective);
  const details = escape(req.body.details);

  const newProfile = new Profile({
    user: req.user.id,
    founder: founder,
    category: category,
    objective: objective,
    details: details
  })
  newProfile.save()
  .then((response)=>{
    console.log(response);
    res.json({success: true, response});
  })
  .catch((err)=>{
    console.log(err);
    res.status(400).json({success: false, err});
  });
});


// get profile by id 
// /api/profile/:profileId
// private 

router.get('/:profileId',passport.authenticate('jwt', { session: false }),(req, res)=>{
  Profile.findById(req.params.profileId)
    .populate('user',['name', 'email', 'avatar'])
    .then((profile)=>{
      console.log(profile);
      res.json({success: true, profile: profile});
    })
    .catch((err)=>{
      console.log(err);
      res.status(400).json({success: false, err});
    })
})

// edit profile 
// /api/profile/edit
// private 

router.put('/edit',passport.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      console.log(profile);
      profile.founder = req.body.founder;
      profile.category = req.body.category;
      profile.objective = req.body.objective;
      profile.details = req.body.details;
      profile.save()
        .then((response)=>{
          console.log(response);
          res.json({success: true, msg : 'Successfully updated'});
        })
        .catch((err)=>{
          console.log(err);
          res.status(400).json({success: false, err});
        });
    })
    .catch((err)=>{
      res.status(404).json({success: false, err});
    })
});


module.exports = router;