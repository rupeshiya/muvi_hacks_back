const express = require('express');
const router = express.Router();
const User = require('../models/Users');
// gravatar for profile pic
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const passport = require('passport');


// test route 
// /api/users/test
router.get('/test', (req, res)=>{
  res.json({success: true, msg: 'test route works'});
});

// register 
// /api/users/register
// public
router.post('/register',(req, res)=>{
  // check if email already exists 
  User.findOne({email: req.body.email})
    .then((user)=>{
      if(user){
         res.json({success: false, msg: 'Email already exists'});
      } else {

        // create avatar 
        const avatar = gravatar.url(req.body.email, {
          s: '200', // size
          r: 'pg', // rating
          d: 'mm' // default
        });

        // create user
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
          avatar: avatar,
          phone: req.body.phone
        });

        // hash password and save user 
        bcrypt.genSalt(10,(err, salt)=>{
          if(err){
            console.log(err);
          } 
          bcrypt.hash(newUser.password, salt, (err, hash)=>{
            if(err){
              console.log(err);
            } else {
              newUser.password = hash;
              newUser.save()
                .then((newUserReg)=>{
                  console.log(newUserReg);
                  res.json({success: true, msg: `Successfully registered ${newUserReg.name}`});
                })
                .catch((err)=>{
                  res.json({success: false, err});
                });
            }
          });
        })
      }
    });
});


// login user 
// /api/user/login 
// public 
router.post('/login', (req, res)=>{
  const email = req.body.email;
  const password = req.body.password;

  // check if user exists or not 
  User.findOne({email: email})
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg : `User does not exist`});
      } else{
        // compare password 
        bcrypt.compare(password, user.password, (err, success)=>{
          if(err){
            res.status(400).json({success: false, msg: 'Password does not matches'});
          }
          if(success){
            // res.json({success: true, msg: 'Successfully loggedin'});
            const payload = { id: user._id, email: user.email, avatar: user.avatar, name: user.name};

            // create token
            jwt.sign(payload, keys.secret, {expiresIn: 3600},(err, token)=>{
              res.json({success: true, token: 'Bearer ' + token});
            })
          }
        });
      }
    })
});




module.exports = router;