const redis = require('redis');
const redisUrl = process.env.redisUrl;
const client = redis.createClient(redisUrl);
//  for returning promise instead of callbacks
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
const mongoose = require('mongoose');

module.exports = client;