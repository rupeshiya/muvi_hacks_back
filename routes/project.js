const express = require('express');
const router = express.Router();
const passport = require('passport');
const Post = require('../models/Posts');
const Profile = require('../models/Profile');
// const { client } = require('../routes/redis');
const redis = require('redis');
const redisUrl = process.env.redisUrl;
const client = redis.createClient(redisUrl);
//  for returning promise instead of callbacks
const bluebird = require('bluebird');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

// SET COLLECTION ON WATCH
// const changeStream  = Post.watch();

// /api/project/test
router.get('/test',(req, res)=>{
  res.json({success: true, msg: 'test route works'});
})


// get all posts 
// /api/project/posts
// public 

router.get('/posts',(req, res)=>{

  // CHECK IF ANY CHANGES IN THE COLLECTIONS
  // changeStream.on('change', ()=>{
  //   console.log('serving from mongoDB');
  //     Post.find({})
  //       .sort('-date')
  //       .populate('user', ['name', 'avatar', 'email'])
  //       .populate('profile')
  //       .exec((err, posts)=>{
  //         if(err){
  //           console.log(err);
  //           res.status(404).json({success: false, err});
  //         } 
  //         if(posts){
  //           // now update the redis cache       
  //           client.set('posts', JSON.stringify(posts), (err, reply)=>{
  //             if(err){
  //               console.log(err);
  //             }
  //             console.log(reply);
  //           });
  //           res.json({success: true, posts: posts});
  //         }
  //       })
  // })
  // check if any hashed data is there in redis 
  client.get('posts',(err, posts)=>{
    if(posts){
      // if yes then send it right away
      console.log('serving from redis ');
      return res.json({success: true, posts: JSON.parse(posts)});
    } else {
      //  if no then send the request and updated the redis with the new data 
      console.log('serving from mongoDB');
      Post.find({})
        .sort('-date')
        .populate('user', ['name', 'avatar', 'email'])
        .populate('profile')
        .exec((err, posts)=>{
          if(err){
            console.log(err);
            res.status(404).json({success: false, err});
          } 
          if(posts){
            // now update the redis cache       
            client.set('posts', JSON.stringify(posts), (err, reply)=>{
              if(err){
                console.log(err);
              }
              console.log(reply);
            });
            res.json({success: true, posts: posts});
          }
        })
      }
    });
});


// get post by id 
// /api/project/post/:postId 
// public 

router.get('/post/:postId',(req, res)=>{
   // check if any hashed data is there in redis 
   client.get(req.params.postId, (err, post)=>{
     if(post){
     // if yes then send it right away
     console.log('serving from redis')
      return res.json({success: true, post: JSON.parse(post)});
    } 
    //  if no then send the request and updated the reds with the new data 
    Post.findById({_id: req.params.postId})
      .populate('user', ['name', 'email', 'avatar'])
      .populate('profile')
      .exec((err, post)=>{
        if(err){
          console.log(err);
          res.status(404).json({success: false , err});
        }
        if(post){
          console.log(post);
           //  update the cacahe 
           client.set(req.params.postId, JSON.stringify(post), (err, reply)=>{
             if (err) {
               console.log(err);
             }
             console.log(reply);
           });
          res.json({success: true, post: post});
        }
      });
   })
});


// create post for the projects 
// /api/project/create 
// private 

router.post('/create',passport.authenticate('jwt',{session: false}),(req, res)=>{
  // check if profile for the user exists or not 
  Profile.findOne({user: req.user.id})
    .then((proifle)=>{
      console.log(proifle);
      if(!profile){
        return res.json({success: false, msg: 'Please create the profile first'});
      } else {
        // create post 
        const newPost = new Post({
          profile: profile._id,
          user: req.user.id,
          title: req.body.title,
          description: req.body.description,
          requiredFunding: req.body.requiredFunding,
          date: Date.now()
        })
        newPost.save()
          .then((post)=>{
            console.log(post);
            // set post in the redis 
            client.set(JSON.stringify(post._id), JSON.stringify(post), (err, reply)=>{
              if (err) {
                console.log(err);
              }
              console.log(reply);
            });
            res.json({success: true, post});
          })
          .catch((err)=>{
            console.log(err);
          })
      }
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json({success: false, err});
    })
});


// like post 
// /api/project/like/:postId
// private 

router.post('/like/:postId',passport.authenticate('jwt',{session: false}),(req, res)=>{
  // check if post exists or not 
  Post.findById(req.params.postId)
    .then((post)=>{
      console.log(post);
      if(!post){
        return res.json({success: false, msg: 'Post does not exists'});
      } 
      // if post exist check if already liked or not 
      if(post.likes.filter(like => like.user.toString() === req.user.id).length > 0){
        res.status(400).json({success: false, msg: 'Already liked by you'});
      } 
      post.likes.unshift({user: req.user.id})
      post.save()
        .then((response)=>{
          console.log(response);
          // update the redis using the latest posts
          client.set(req.params.postId, response, (err, reply)=>{
            if (err) {
              console.log(err);
            }
            console.log(reply);
          });
          res.json({success: true, msg: 'Successfully liked'});
        })
        .catch((err)=>{
          console.log(err);
        });
    })
    .catch((err)=>{
      res.status(404).json({success: false, err});
    });
});

// unlike post 
// /api/project/unlike/:postId
// private 

router.post('/unlike/:postId',passport.authenticate('jwt',{session: false}),(req, res)=>{
  Post.findById(req.params.postId)
    .then((post)=>{
      console.log(post);
      // check if liked or not 
      if(post.likes.filter(like => like.user.toString() === req.user.id).length === 0){
        return res.status(400).json({ notliked: 'You have not yet liked this post' });
      }
      const likedUserId = post.likes.map((like => like.user.toString()));
      const likedIndex = likedUserId.indexOf(req.user.id);
      post.likes.splice(likedIndex, 1);
      post.save()
        .then((response)=>{
          console.log(response);
          // update the redis  database 
          client.set(req.params.postId, response, (err, reply)=>{
            if (err) {
              console.log(err);
            }
            console.log(reply);
          });
          res.json({success: true, msg: 'Successfully disliked'});
        })
        .catch((err)=>{
          console.log(err);
        })
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json({success: false, err});
    });
});

// comment 
// /api/project/comment/:postId
// private 

router.post('/comment/:postId',passport.authenticate('jwt',{session: false}),(req, res)=>{
  Post.findById(req.params.postId)
    .then((post)=>{
      console.log(post);
      const commentObj = {
        user: req.user.id,
        avatar: req.user.avatar,
        text: req.body.text,
        date: Date.now()
      };
      post.comments.unshift(commentObj)
      post.save()
      .then((response)=>{
        console.log(response);
        // update the redis database 
        client.set(req.params.postId, response, (err, reply)=>{
          if (err) {
            console.log(err);
          }
          console.log(reply);
        });
        res.json({success: true, msg: 'Successfully added comment '});
      })
      .catch((err)=>{
        console.log(err);
      });
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json({success: false, err});
    });
});

// delete posts by id 
// /api/project/posts/:postId
// private 
router.delete('/posts/:postId', passport.authenticate('jwt',{session: false}), (req, res)=>{
  Post.findByIdAndRemove(req.params.postId)
    .then((post)=>{
      console.log(`post ${post._id} deleted `);
      res.json({success: true, msg: 'Deleted '});
    })
    .catch((err)=>{
      console.log(err);
      return res.status(404).json({success: false, err});
    });
});


module.exports = router;