const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  profile:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'profile'
  },
  title:{
    type: String,
    required: true
  },
  description:{
    type: String,
    required: true
  },
  youtubelink:{
    type: String
  },
  requiredFunding:{
    type: String,
    required: true
  },
  fundingRaised:{
    type: Number
  },
  likes:[
    {
      user:{
        type: Schema.Types.ObjectId,
        ref: 'users'
      }
    }
  ],
  comments:[
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
      name:{
          type: String
      },
      avatar: {
        type: String
      },
      text:{
        type: String,
        required: true
      },
      date:{
        type: Date,
        default: Date.now()
      }
    }
  ],
  // to do 
  // image:{
  //   type: String
  // }
  date:{
    type: Date,
    default: Date.now()
  }
})
module.exports = project = mongoose.model('post', postSchema);