const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profileschema = new Schema({
  user:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  founder:{
    type: String,
    required: true
  },
  category:{
    type: String,
    required: true
  },
  objective:{
    type: String,
    required: true
  },
  details:{
    type: String,
    required: true
  }
})
module.exports = profile = mongoose.model('profile', profileschema);